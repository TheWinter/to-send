var r = [8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,256 ]
var g = [8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,256 ]
var b = [8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,256 ]

var combinations = []

/**  returns an array of arrays containing all r g b combinations
   * using above arrays as input
    */
function colourGenerator(...args) {
    var max = args.length-1;
    function helper(arr, i) {
        for (var j=0, l=args[i].length; j<l; j++) {
            var a = arr.slice(0); // clone arr
            a.push(args[i][j]);
            if (i==max)
                combinations.push(a);
            else
                helper(a, i+1);
        }
    }
    helper([], 0);
    return combinations;
}

/**  loops over each r g b combination and fills the canvas */
function addColours(){
    var yinit = 0;
    var x = 0, y = 0;
    var canvas = document.getElementById('colourSquare');
    var ctx = canvas.getContext('2d');
    for (var i=0; i < combinations.length; i++) {
        c =combinations[i]
        ctx.fillStyle = "rgba("+c[0]+","+c[1]+","+c[2]+")";
        ctx.fillRect( x, y, 1, 1 );
        if (x == 255) {
            x = 0;
            y = yinit + 1;
            yinit += 1;
        } else {
            x += 1;
        }
    }
}

/**  function to shuffle the order of r g b arrays in the
   * combinations array
    */
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }

  /** shuffles the combinations array and paints the
   * canvas
    */
function randomColours(){
    shuffle(combinations)
    addColours()
}

